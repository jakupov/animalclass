﻿#include <iostream>
#include "Dog.h"
#include "Cat.h"
#include "Cow.h"

int main()
{
    Animal* Animals[3];
    Animals[0] = new Dog();
    Animals[1] = new Cat();
    Animals[2] = new Cow();

    for (auto elem : Animals)
    {
        elem->Voice();
    }
}